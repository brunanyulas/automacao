before (() => {
    cy.visit('https://demo.realworld.io/#/register')
})

it ('Cadastro de usuário com credenciais válidas', () => {
    cy.get('[ng-model*=username]').type('docemarca10')
    cy.get('[ng-model*=email]').type('docemarca10@gmail.com')
    cy.get('[ng-model*=password]').type('docemarca10')
    cy.contains('button', 'Sign up').click()
    cy.get('[ng-show*=loading]').should('contain', 'No articles are here... yet.')
});

it.only ('Cadastro de usuário com e-mail já cadastrado', () => {

    cy.intercept({
        method: 'POST',
        pathname: '/api/users'
    }, {
        statusCode: 422,
        fixture: 'cadastro-email-usado'
    }).as('postUsers')

    cy.get('[ng-model*=username]').type('docemarca20')
    cy.get('[ng-model*=email]').type('docemarca@gmail.com')
    cy.get('[ng-model*=password]').type('docemarca20')
    cy.contains('button', 'Sign up').click()
   // cy.get('[ng-repeat*=errors]').should('contain', 'email has already been taken')
    
   cy.wait(10000)
    cy.wait('@postUsers')

});

it("Cadastro com usuário já cadastrado", ()=> {
    cy.get("[ng-model*=username]").type("docemarca")
    cy.get("[ng-model*=email]").type("docemarca50@gmail.com")
    cy.get("[ng-model*=password]").type("docemarca20")
    cy.contains("button","Sign up").click()
    cy.get("[ng-repeat*=errors]").should("contain", "username has already been taken")
});



